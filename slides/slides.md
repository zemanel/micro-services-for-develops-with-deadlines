---
title: Microservices for developers with deadlines
hideFirstSlide: true
authors: José Moreira
email: "jose.moreira@container-solutions.com"
---

# Bio

* Software Engineer @ Container Solutions
* Full-stack developer but mostly backend
* Passionate about architecture and infrastructure
* ❤ containers

---

## Agenda

* Monoliths
* Microservices
* Standards, consistency & automation
* APIs: the glue between microservices
* Local and remote development on Kubernetes clusters
* Questions?

---

## 👽 Monolith architecture

---

** Monolith architecture **

* Single code base/repository
* Single deployment pipeline
* Simpler development (single code base)

---

## 🐙 Microservice architecture

---

** Microservice architecture **

* Many code bases/repositories (if not using `monorepo`)
* Multiple independent deployment pipelines
* Simpler development (smaller scope)

---

Small overhead * number of microservices = 🤯

---

* Copy pasted boilerplate (ex: `.gitlab-ci.yml`) across repos
* Manual documentation can get outdated (**spoiler**: it will!)
* Consistency over time can be hard

Note:
If having metadata deployment configuration files on repo (`.gitlab-ci.yml`)

---

## Create/update boilerplate with generators

![Cookie-cutter](images/cookie-cutter.png)

---

## APIs are the glue between microservices

---

## OpenAPI / Swagger

* https://www.openapis.org
* https://swagger.io

---

```yaml
# api-spec.yaml
# <...>
paths:
  /pets:
    get:
      summary: List all pets
      operationId: listPets
      tags:
        - pets
      parameters:
        - name: limit
          in: query
          description: How many items to return at one time (max 100)
          required: false
          schema:
            type: integer
            format: int32
```

---

## From API specification, we can generate

* Documentation
* Client/server boilerplate
* Mock HTTP responses from specification (testing)

---

## Generate API specification from source code

* <https://github.com/axnsan12/drf-yasg> (Django Rest Framework )

* <https://github.com/springfox/springfox> (Spring Boot)

---

**Major time saver!**

* DRY (Don't Repeat Yourself)
* Keeps documentation in sync with code!
* Microservice consumers can start writing code against mock servers

---

### Microservice local Development

* Production-like environments locally is 🤯
* Kubernetes streamlines operations 👍

---

** Easy cluster setup with Minikube **

```bash
$ minikube start --vm-driver=hyperkit
Starting local Kubernetes v1.10.0 cluster...
Starting VM...
Downloading Minikube ISO
 170.78 MB / 170.78 MB [============================================] 100.00% 0s
Getting VM IP address...
Moving files into cluster...
Downloading kubelet v1.10.0
Downloading kubeadm v1.10.0
Finished Downloading kubeadm v1.10.0
Finished Downloading kubelet v1.10.0
Setting up certs...
Connecting to cluster...
Setting up kubeconfig...
Starting cluster components...
Kubectl is now configured to use the cluster.
Loading cached images from config file.
```

---

** Easy system overview **

```bash
$ kubectl get pods --watch=true --namespace=development

NAME                                     READY   STATUS    RESTARTS   AGE
accounts-deployment-7d687f4868-45tpm     1/1     Running   0          5h
orders-deployment-7d687f4868-cg25l       1/1     Running   2          5h
tracking-deployment-7d687f4868-wqrnt     1/1     Running   0          5h
```

---

** #protip **

Let container processes crash on connection failures:

* so that process status == pod health (pod health monitoring)
* Kubernetes will manage restarts

---

Get logs from specific microservices

```bash
$ kubectl logs --selector="app=nginx"

66.249.65.159 - - [06/Nov/2014:19:10:38 +0600] "GET /news/53f8d72920ba2744fe873ebc.html HTTP/1.1" 404 177 "-" "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
66.249.65.3 - - [06/Nov/2014:19:11:24 +0600] "GET /?q=%E0%A6%AB%E0%A6%BE%E0%A7%9F%E0%A6%BE%E0%A6%B0 HTTP/1.1" 200 4223 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
66.249.65.62 - - [06/Nov/2014:19:12:14 +0600] "GET /?q=%E0%A6%A6%E0%A7%8B%E0%A7%9F%E0%A6%BE HTTP/1.1" 200 4356 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"

```

---

## Little community tools, big time savings!

<https://github.com/ahmetb/kubectx>

* `kubectx` helps you switch between clusters back and forth

```bash
$ kubectx minikube
Switched to context "minikube".
```

* `kubens` helps you switch between Kubernetes namespaces smoothly

```bash
$ kubens kube-system
Context "test" set.
Active namespace is "kube-system".
```

---

## But the whole stack resource requirements might not fit your humble developer laptop

🥺 I'm not saying i want a better laptop, but not saying i don't either

---

## Developing against remote cluster

`kubectl` supports local port forwarding

```bash
# Listen on ports 5000 and 6000 locally
# forwarding data to/from ports 5000 and 6000 in the pod
$ kubectl port-forward pod/mypod 5000 6000
```

but ...

---

![Telepresence](images/bird-on-bricks.svg)

<https://www.telepresence.io>

---

## Telepresence

* Deploys a two-way network proxy in a Kubernetes cluster pod
* Runs your code locally, as a normal local process
* Forwards requests to/from the Kubernetes cluster
* Remote (private) services have full access to the local service

```bash
$ brew install socat datawire/blackbird/telepresence
```

---

```bash
$ docker build . -t end-to-end-tests:latest
$ telepresence --docker-run -it end-to-end-tests:latest sh
# I'm a local shell and can connect to remote services
/usr/src/app $ curl http://remote-private-service:8080/api/ping
HTTP 200 OK
```

---

**Telepresence helped me a lot!**

* Developed integration/end-to-end tests locally
* Editor step-by-step debugging breakpoints

---

![Hiring](images/cs-hiring.png)

---

## Questions

---

## Thank you!

* Twitter: @zemanel
* E-mail: jose.moreira@container-solutions.com
* LinkedIn: https://www.linkedin.com/in/josemoreira
